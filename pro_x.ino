//%% #define RTC_1302_ENABLED
//%% #define LCD_I2C_ENABLED
//%% #define RTC_1307_ENABLED
//%% #define DHT_ENABLED
//%% #define BMP_180_ENABLED

//%% #include "profile.h"
#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>
//%% #include <tlib.h>
//%% #include <serial_protocol.h>

//***** Пин бипера
//%% #define BEEPERPIN                 12

//int test = 0;

//***** Светодиод индикации событий
#ifdef INDICATOR_ENABLED
#define INDICATOR_LED_PIN          9
#endif

//***** Часы реального времени
#ifdef RTC_1307_ENABLED
//*** Подключение
// AT24C32 RTC I2C dip-ds1307 
// GND - на землю(Ч), 
// VCC - питание(Кор), 
// SDA - Analog 4(Кр), 
// SCL - Analog 5(Ор), 
// DS - никуда(Ж)
//*** Библиотеки
#include <DS1307RTC.h>
#include <TimeLib.h>
//*** Данные
#define      RTC_PROTOCOL_ID     "%C"
#define      CLOCKBUFSIZE        20
DS1307RTC    rtc=DS1307RTC();
tmElements_t timerec;
char         clockbuffer[CLOCKBUFSIZE];
const char   *monthName[12] = {
  "Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};
#endif

//***** Датчик температуры и влажности
#ifdef DHT_ENABLED 
//*** Подключение
// "-" - на землю(Ч), 
// OUT - на цифровой вход Arduino
// "+" - питание(Кор), 
// "+" и OUT соединить резистором 10кОм
//*** Библиотеки
#include <DHT.h>
//*** Пины
#define DHT_PIN  7
#define DHT_TYPE DHT22  
//*** Данные
DHT dht(DHT_PIN, DHT_TYPE);
float temperature;
float humidity;
#endif

//***** Жидкокристаллический дисплей
#ifdef LCD_I2C_ENABLED
//*** Подключение
// GND - на землю
// VCC - на питание
// SDA - 4 аналоговый вход
// SCL - 5 аналоговый вход
//*** Библиотеки
#include <LiquidCrystal_I2C.h>
//*** Адрес
#define I2C_LCD_DISPLAY_ADDRESS 0x27
//*** Данные
#define CLOCK_COL   0
#define CLOCK_ROW   0
LiquidCrystal_I2C *lcd=NULL;
#endif

//***** RTC 1302
#ifdef  RTC_1302_ENABLED
//*** Библиотеки
#include <DS1302RTC.h>
//*** Пины
#define RTC_RST_PIN 5
#define RTC_DAT_PIN 6
#define RTC_CLK_PIN 7
#define RTC_GND_PIN 8
#define RTC_VCC_PIN 9
//*** Переменные
DS1302RTC *rtc=NULL;
int       rtc_year=0;
int       rtc_month=0;
int       rtc_day=0;
int       rtc_hour=0;
int       rtc_minute=0;
int       rtc_second=0;
char      clockbuffer[20];
time_t rtc_time;
tmElements_t timerec;
#endif

//***** Барометр
#ifdef BMP_180_ENABLED
#include <BMP085.h>
//*** Подключение
// VIN - 3,3 В !!!
// GND - на землю
// SCL - 5 аналоговый вход
// SDA - 4 аналоговых вход
BMP085 dps = BMP085();    
long Temperature = 0;
long Pressure = 0;
#endif

//***** Сенсорная кнопка
#ifdef TTP223B_ENABLED
#define TTP223B_PIN                      8
#define TTP223B_TIME_IN_PRESSED_STATE 2000
int ttp223b_pressed = 0;
long unsigned int ttp223b_pressed_time = 0;
#endif

//***** Cигнальный светодиод
#define CONTROL_LED_PIN           13
#define CONTROL_LED_GLOW_TIME   1000
int control_led_state              = 0;
long unsigned int control_led_time = 0;

//***** Последовательный порт
#define SERIAL_INCOME_BUFFER_SIZE       64
int querySerial();

char serial_income_buffer[SERIAL_INCOME_BUFFER_SIZE];

void setup() {
    
    //%% pinMode(BEEPERPIN, OUTPUT);
    //%% pinMode(CONTROL_LED_PIN, OUTPUT);
    //%% control_led_time=millis();

    //***** Последовательный порт
    Serial.begin(9600);
    while (!Serial); // wait until Arduino Serial Monitor opens
  
    Wire.begin(); 
    
    //***** LCD дисплей
    #ifdef LCD_I2C_ENABLED
    lcd=initializeLCDDisplay(I2C_LCD_DISPLAY_ADDRESS);
    lcd->clear();   
    lcd->print(">"); 
    lcd->moveCursorRight();
    displayText(0,1,"Initialization...",500);
    lcd->clear();
    #endif
    
    //***** RTC 1302 часы
    #ifdef  RTC_1302_ENABLED
    rtc=initializeRTCClock(RTC_RST_PIN, RTC_DAT_PIN, RTC_CLK_PIN, RTC_GND_PIN, RTC_VCC_PIN);    
    #endif
    
    //***** DHT датчик
    #ifdef DHT_ENABLED
    dht.begin();
    #endif
    
    //***** RTC 1307
    #ifdef RTC_1307_ENABLED
    ///*
    rtc.begin();
    rtc.adjust(DateTime(__DATE__, __TIME__));  
    if (getDate(__DATE__) && getTime(__TIME__)) {
      if (RTC.write(timerec)) {
        Serial.println("RTC time setted");
      }
    }
    //
    #endif
    
    //*****  BMP 180
    #ifdef BMP_180_ENABLED
    dps.init();
    #endif 

    #ifdef TTP223B_ENABLED
    pinMode(TTP223B_PIN, INPUT);     
    pinMode(INDICATOR_LED_PIN, OUTPUT);     
    ttp223b_pressed_time=millis();
    #endif


}

//***** Текущее состояние сигнального светодиода
byte signalstate=0;

void loop() {


    //***** Запрашиваем данные датчика температуры и давления
    #ifdef DHT_ENABLED 
    if(readDHTData()) {;
        
        displayDHTData();
    }
    #endif 
    
    //***** Запрашиваем данные часов
    #ifdef RTC_1307_ENABLED
    //if(readRTCData()) {
        readRTCData(); 
        displayRTCData();
    //}
    #endif
    
    
    #ifdef BMP_180_ENABLED
    dps.getPressure(&Pressure); 
    dps.getTemperature(&Temperature);
    Serial.print("Temp: ");
    Serial.print(Temperature);
    Serial.print(" Press ");
    Serial.println((float)Pressure*3.213);
    //Serial.println(Pressure);
    #endif 
    
    
    #ifdef TTP223B_ENABLED
    //***** Читаем состояние кнопки
    if(digitalRead(TTP223B_PIN)) {
      
        ttp223b_pressed=1;
        ttp223b_pressed_time=millis();
        
        #ifdef INDICATOR_ENABLED
        digitalWrite(INDICATOR_LED_PIN,HIGH);
        #endif
        
        Serial.println("Sensor buton was pressed!");
    }

    //***** индикатор
    if(ttp223b_pressed && millis()>ttp223b_pressed_time+TTP223B_TIME_IN_PRESSED_STATE) {
      
        ttp223b_pressed=0;
        #ifdef INDICATOR_ENABLED
        digitalWrite(INDICATOR_LED_PIN,LOW);
        #endif
        Serial.println("Sensor buton was unpressed!");
    }
    #endif

    //***** Запрашиваем данные из последовательного порта
 
    if(querySerial()) {
        
    }

    //Serial.println("Ping."); 
    //flashSignal(0);
    //beep(BEEPERPIN,1000,1);
    
};

/*
void flashSignal( int pause) {

    / / ***** контрольный светодиод 
    if(millis()>control_led_time+CONTROL_LED_GLOW_TIME) {
       
        control_led_time=millis();  
        if(control_led_state==HIGH) {
 
            digitalWrite(CONTROL_LED_PIN, LOW); 
              control_led_state=LOW;
        } else {
 
            digitalWrite(CONTROL_LED_PIN, HIGH);
            control_led_state=HIGH;
        }
    }
};
*/

#ifdef LCD_I2C_ENABLED
LiquidCrystal_I2C* initializeLCDDisplay(int address) {
    
    lcd=new LiquidCrystal_I2C(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
    //?lcd=new LiquidCrystal_I2C(address); //, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
    lcd->begin(16,2);
    lcd->backlight();// Включаем подсветку дисплея
    delay(500);
    return lcd;
};
#endif


#ifdef  RTC_1302_ENABLED
DS1302RTC* initializeRTCClock(int resetpin, int datapin, int clockpin, int gndpin, int vccpin) {
 
    pinMode(gndpin, OUTPUT);
    digitalWrite(gndpin, LOW);
    pinMode(vccpin, OUTPUT);
    digitalWrite(vccpin, HIGH);

    rtc=new DS1302RTC(resetpin, datapin, clockpin);
    
    if (rtc->haltRTC()) {
        displayText(0,1,"RTC stopped!",1000);
    } else {
        displayText(0,1,"RTC working.",1000);
    }    
    setSyncProvider(rtc->get); // the function to get the time from the RTC
    if(timeStatus() == timeSet)
        displayText(0,1,"Proceed!",1000);
    else
        displayText(0,1,"Failed!",1000);
    return rtc;
};    


void readRTCData() {

  rtc_year=year();
  rtc_month=month();
  rtc_day=day();
  rtc_hour=hour();
  rtc_minute=minute();
  rtc_second=second();
}


void writeRTCData(){

  timerec.Year=y2kYearToTm(rtc_year-2000);
  timerec.Month=rtc_month;
  timerec.Day=rtc_day;
  timerec.Hour=rtc_hour;
  timerec.Minute=rtc_minute;
  timerec.Second=rtc_second;
  
  rtc_time=makeTime(timerec);
  if(rtc->set(rtc_time)==0) {
     
    setTime(rtc_time);  
  }
}


void displayClock() {
    
    readRTCData();
    lcd->setCursor(CLOCK_COL,CLOCK_ROW);
    sprintf(clockbuffer,"%02d/%02d/%4d %02d:%02d:%02d",day(),month(),year(),hour(),minute(),second());  
}
#endif


#ifdef DHT_ENABLED
int readDHTData() {

    temperature=dht.readTemperature();
    humidity=dht.readHumidity();
    return (isnan(temperature) || isnan(humidity))?0:1;
}


void displayDHTData() {
    char message[32];
    int temp_int,temp_frac, hum_int, hum_frac;
    temp_int=temperature;
    temp_frac=getFractPart(temperature,100);    
    hum_int=humidity;
    hum_frac=getFractPart(humidity,100);    
    sprintf(message,"T: %2d.%02d C H: %2d.%02d \%",
      temp_int,temp_frac, hum_int, hum_frac);
    displayText(0,1,message,0);
}
#endif


#ifdef RTC_1307_ENABLED
int readRTCData() {
    
    //Serial.println("query RTC");
    return rtc.read(timerec);
}


void displayRTCData() {
    
    char clockbuffer[20];
    memset(clockbuffer,0,CLOCKBUFSIZE);      
    /*
    sprintf(clockbuffer,"%02d/%02d/%2d %02d%02d%02d",timerec.Day, timerec.Month,
      (timerec.Year-30), timerec.Hour, timerec.Minute, timerec.Second);  
    */
    sprintf(clockbuffer,"%02d/%02d/%2d",timerec.Day, timerec.Month,(timerec.Year-30));  

    displayText(0,0,clockbuffer,1);  
    //Serial.println(timerec.Year);
}


bool getTime(const char *str) {
    int Hour, Min, Sec;

    if (sscanf(str, "%d:%d:%d", &Hour, &Min, &Sec) != 3) 
      return false;
    timerec.Hour = Hour;
    timerec.Minute = Min;
    timerec.Second = Sec;
    return true;
}


bool getDate(const char *str) {
    char Month[12];
    int Day, Year;
    uint8_t monthIndex;

    if (sscanf(str, "%s %d %d", Month, &Day, &Year) != 3) 
        return false;
    for (monthIndex = 0; monthIndex < 12; monthIndex++) {
        if (strcmp(Month, monthName[monthIndex]) == 0) break;
    }
    if (monthIndex >= 12) 
        return false;
    timerec.Day = Day;
    timerec.Month = monthIndex + 1;
    timerec.Year = CalendarYrToTm(Year);
    return true;
}

#endif


void beep(int pin, int freq, int playtime) {
    
    int period=1000000/freq;
    int halfofperiod = period>>1;
    int cycles=(playtime*1000000)/period;
    for(int times=0;times<cycles;times++) {
        
        digitalWrite(pin, HIGH);
        delayMicroseconds(halfofperiod);
        digitalWrite(pin, LOW);
        delayMicroseconds(halfofperiod>>1);    
    }
};


void displayText(int col, int row, const char *message, int delayvalue) {
    
    #ifdef  LCD_I2C_ENABLED
    lcd->setCursor(col,row);
    lcd->print(message);
    if(delayvalue>0)
      delay(delayvalue);
    #else        
    Serial.println(message);
    #endif
};


int querySerial() {
    
    if(Serial.available()) {
        
        memset(serial_income_buffer,0,SERIAL_INCOME_BUFFER_SIZE);
        Serial.readBytes(serial_income_buffer, SERIAL_INCOME_BUFFER_SIZE);
        
        
        #ifdef RTC_1307_ENABLED
        //if(recognizeIdentifier(serial_income_buffer,RTC_PROTOCOL_ID)) {
            //***** Нам посылка!
            
        //}
        #endif
        return 1;
    } else {

        return 0;
    }
}
